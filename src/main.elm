module Main exposing (fetchConfig, getView, init, main, subscriptions, update, view)

import Browser
import ChallengeService exposing (..)
import ClueService exposing (..)
import ConfigurationPage exposing (..)
import CountdownService exposing (..)
import Element exposing (..)
import Element.Background as Background
import Element.Font as Font
import EndingPage exposing (..)
import GameConfigDecoder exposing (..)
import GameConfigService exposing (defaultGameConfig)
import GamePage exposing (..)
import Html exposing (Html)
import Http
import LandingPage exposing (..)
import Messages exposing (..)
import Model exposing (Model)
import Step exposing (..)
import StepService exposing (..)
import Time
import VideoPage exposing (..)


main : Program () Model Msg
main =
    Browser.document
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


init : flags -> ( Model, Cmd Msg )
init flags =
    ( { step = firstStep
      , selectedLocation = ""
      , selectedNumberOfPlayers = ""
      , gameConfig = defaultGameConfig
      , selectedGameConfig = Nothing
      , currentChallengeIndex = 0
      , password = ""
      , isPasswordInvalid = False
      , timestampAtStart = Nothing
      , timeDifference = Nothing
      , availableClues = []
      }
    , fetchConfig
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NextStep ->
            ( { model | step = getNextStep model.step }, getCurrentTime )

        GotGameConfig result ->
            case result of
                Ok gameConfig ->
                    ( { model | gameConfig = gameConfig }, Cmd.none )

                Err _ ->
                    ( model, Cmd.none )

        SelectLocation location ->
            ( { model | selectedLocation = location }, getCurrentTime )

        SelectNumberOfPlayers number ->
            ( { model | selectedNumberOfPlayers = number }, getCurrentTime )

        SubmitConfiguration ->
            let
                selectedGameConfig =
                    List.filter
                        (\config ->
                            config.location
                                == model.selectedLocation
                                && config.numberOfPlayers
                                == model.selectedNumberOfPlayers
                        )
                        model.gameConfig.gameConfigs
                        |> List.head

                step =
                    if
                        model.selectedLocation
                            /= ""
                            && model.selectedNumberOfPlayers
                            /= ""
                    then
                        getNextStep model.step

                    else
                        model.step
            in
            ( { model | step = step, selectedGameConfig = selectedGameConfig }, Cmd.none )

        VideoEnded ->
            ( { model | step = getNextStep model.step }, getCurrentTime )

        PasswordEntered password ->
            ( { model | password = password, isPasswordInvalid = False }, Cmd.none )

        SubmitPassword ->
            let
                currentChallenge =
                    getCurrentChallenge model.selectedGameConfig model.currentChallengeIndex

                modelUpdate =
                    if model.password == currentChallenge.password then
                        { step = getNextStepInGame model.step model.selectedGameConfig model.currentChallengeIndex
                        , isPasswordInvalid = False
                        , currentChallengeIndex = getNextChallengeIndex model.currentChallengeIndex
                        , password = ""
                        , availableClues = []
                        , cmd = getCurrentTime
                        }

                    else
                        { step = model.step
                        , isPasswordInvalid = True
                        , currentChallengeIndex = model.currentChallengeIndex
                        , password = model.password
                        , availableClues = model.availableClues
                        , cmd = Cmd.none
                        }
            in
            ( { model
                | step = modelUpdate.step
                , isPasswordInvalid = modelUpdate.isPasswordInvalid
                , currentChallengeIndex = modelUpdate.currentChallengeIndex
                , password = modelUpdate.password
                , availableClues = modelUpdate.availableClues
              }
            , modelUpdate.cmd
            )

        CurrentTime now ->
            ( { model | timestampAtStart = Just now }, Cmd.none )

        GetClue ->
            let
                cmd =
                    if List.length (getCurrentClues model) == List.length model.availableClues then
                        Cmd.none

                    else
                        getCurrentTime
            in
            ( model, cmd )

        Tick now ->
            let
                timestampAtStart =
                    case model.timeDifference of
                        Just time ->
                            if time == 0 then
                                Nothing

                            else
                                model.timestampAtStart

                        Nothing ->
                            model.timestampAtStart

                timeDifference =
                    case model.timeDifference of
                        Just time ->
                            if time == 0 then
                                Nothing

                            else
                                calculateTimeDifference model.gameConfig.timerForCluesInMinutes model.timestampAtStart now

                        Nothing ->
                            calculateTimeDifference model.gameConfig.timerForCluesInMinutes model.timestampAtStart now

                availableClues =
                    case timeDifference of
                        Just timeDelta ->
                            if timeDelta == 0 then
                                setNextClueAvailable
                                    (getCurrentClues model)
                                    model.availableClues

                            else
                                model.availableClues

                        Nothing ->
                            model.availableClues
            in
            ( { model | timestampAtStart = timestampAtStart, timeDifference = timeDifference, availableClues = availableClues }, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Time.every 500 Tick


view : Model -> Browser.Document Msg
view model =
    { title = "Hermandad Lambda"
    , body =
        [ Element.layout [ padding 50, width fill, Background.color (rgb 0 0 0), Font.color (rgb 1 1 1) ]
            (getView model)
        ]
    }


getView : Model -> Element Msg
getView model =
    case model.step of
        Configuration ->
            configurationView model

        Landing ->
            landingView model

        Intro ->
            videoView model model.gameConfig.videoIntroSrc

        Game ->
            let
                challenge =
                    case model.selectedGameConfig of
                        Just gameConfigElement ->
                            getChallengeByIndex gameConfigElement.challenges model.currentChallengeIndex

                        Nothing ->
                            defaultChallenge
            in
            gameView model challenge

        Outro ->
            videoView model model.gameConfig.videoOutroSrc

        Ending ->
            endingView model


fetchConfig : Cmd Msg
fetchConfig =
    Http.get
        { url = "game.json"
        , expect = Http.expectJson GotGameConfig gameConfigDecoder
        }
