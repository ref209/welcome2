module CountdownService exposing (..)

import Messages exposing (..)
import Task
import Time


minuteInMillis =
    60000


calculateTimeDifference : Int -> Maybe Time.Posix -> Time.Posix -> Maybe Int
calculateTimeDifference timerInMinutes timestampAtStart now =
    case timestampAtStart of
        Just time ->
            let
                timeDelta =
                    (timerInMinutes * minuteInMillis) - (Time.posixToMillis now - Time.posixToMillis time)
            in
            if timeDelta > 0 then
                Just timeDelta

            else
                Just 0

        Nothing ->
            Nothing


getCurrentTime : Cmd Msg
getCurrentTime =
    Task.perform CurrentTime Time.now
