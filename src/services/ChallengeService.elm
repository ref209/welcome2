module ChallengeService exposing (..)

import Array
import Challenge exposing (Challenge)
import GameConfigElement exposing (GameConfigElement)


isLastChallenge : Maybe GameConfigElement -> Int -> Bool
isLastChallenge gameConfigElement currentChallengeIndex =
    case gameConfigElement of
        Just configElement ->
            currentChallengeIndex >= List.length configElement.challenges - 1

        Nothing ->
            False


getCurrentChallenge : Maybe GameConfigElement -> Int -> Challenge
getCurrentChallenge gameConfigElement currentChallengeIndex =
    case gameConfigElement of
        Just gameConfig ->
            Maybe.withDefault defaultChallenge (Array.get currentChallengeIndex (Array.fromList gameConfig.challenges))

        Nothing ->
            defaultChallenge


defaultChallenge : Challenge
defaultChallenge =
    { description = ""
    , password = ""
    , clues = []
    }


getNextChallengeIndex : Int -> Int
getNextChallengeIndex currentIndex =
    currentIndex + 1


getChallengeByIndex : List Challenge -> Int -> Challenge
getChallengeByIndex challenges index =
    let
        maybeChallenge =
            Array.fromList challenges
                |> Array.get index
    in
    Maybe.withDefault defaultChallenge maybeChallenge


getCurrentGameConfigChallenge : Maybe GameConfigElement -> Int -> Challenge
getCurrentGameConfigChallenge gameConfigElement challengeIndex =
    case gameConfigElement of
        Just gameConfig ->
            getChallengeByIndex gameConfig.challenges challengeIndex

        Nothing ->
            defaultChallenge
