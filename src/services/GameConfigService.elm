module GameConfigService exposing (..)

import GameConfig exposing (GameConfig)

defaultGameConfig : GameConfig
defaultGameConfig =
    { locations = []
    , numberOfPlayers = []
    , videoIntroSrc = ""
    , videoOutroSrc = ""
    , timerForCluesInMinutes = 1
    , gameConfigs =
        []
    }
