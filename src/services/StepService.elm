module StepService exposing (..)

import ChallengeService exposing (isLastChallenge)
import GameConfigElement exposing (GameConfigElement)
import Step exposing (..)


firstStep : Step
firstStep =
    Configuration


getNextStepInGame : Step -> Maybe GameConfigElement -> Int -> Step
getNextStepInGame step gameConfigElement currentChallengeIndex =
    if isLastChallenge gameConfigElement currentChallengeIndex then
        getNextStep step

    else
        step


getNextStep : Step -> Step
getNextStep step =
    case step of
        Configuration ->
            Landing

        Landing ->
            Intro

        Intro ->
            Game

        Game ->
            Outro

        Outro ->
            Ending

        Ending ->
            Ending
