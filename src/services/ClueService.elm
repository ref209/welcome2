module ClueService exposing (..)

import Model exposing (Model)
import Clue exposing (Clue)
import ChallengeService exposing (getCurrentGameConfigChallenge)
import Array

setNextClueAvailable : List Clue -> List Clue -> List Clue
setNextClueAvailable clues availableClues =
    case Array.get (List.length availableClues) (Array.fromList clues) of
        Just element ->
            List.append availableClues [ element ]

        Nothing ->
            availableClues

getCurrentClues : Model -> List Clue
getCurrentClues model =
    (getCurrentGameConfigChallenge model.selectedGameConfig model.currentChallengeIndex).clues
