module ConfigurationPage exposing (..)

import Model exposing (Model)
import Element exposing (..)
import Element.Input as Input
import Element.Background as Background
import Element.Font as Font
import Html exposing (Html)
import Messages exposing (..)

configurationView : Model -> Element Msg
configurationView model =
        column [ spacing 20 ]
            [ row []
                [ el [] (text "Selecciona la localizacion del juego:")
                ]
            , row [ centerX, spacing 20 ]
                (List.map
                    (\location -> Input.button [] { onPress = Just (SelectLocation location), label = text location })
                    ( List.map (\ config -> config) model.gameConfig.locations )
                )
            , row []
                [ el [] (text "Selecciona el numero de jugadores:")
                ]
            , row [ centerX, spacing 20 ]
                (List.map
                    (\number -> Input.button [] { onPress = Just (SelectNumberOfPlayers number), label = text number })
                    ( List.map (\ config -> config) model.gameConfig.numberOfPlayers )
                )
            , row [ centerX, spacing 20 ]
                [ Input.button []
                    { onPress = Just SubmitConfiguration, label = text "Empezar" }
                ]
            ]
