module LandingPage exposing (landingView)

import Element exposing (..)
import Element.Font as Font
import Element.Input as Input
import Model exposing (Model)
import Messages exposing (..)


landingView : Model -> Element Msg
landingView model =
    column [ centerX, height fill, spaceEvenly ]
        [ row [ centerX ]
            [ el [] (text "Hermandad Lambda")
            ]
        , row [ centerX ]
            [ el [ Font.size 300 ] (text "λ")
            ]
        , row [ centerX ]
            [ Input.button [] { onPress = Just NextStep, label = text "Comenzar" }
            ]
        ]
