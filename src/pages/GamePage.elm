module GamePage exposing (..)

import Challenge exposing (Challenge)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Input as Input
import Events exposing (..)
import Html exposing (Html)
import Html.Attributes as Attributes
import Html.Events as Events
import Messages exposing (..)
import Model exposing (Model)
import Time


gameView : Model -> Challenge -> Element Msg
gameView model challenge =
    column [ centerX, height fill, spaceEvenly ]
        [ row [ centerX ]
            [ paragraph [] [ text challenge.description ]
            ]
        , row [ centerX ]
            [ Input.text [ Border.color (getInputColor model.isPasswordInvalid), Background.color (rgb 0 0 0), onEnter SubmitPassword, centerX ] { onChange = PasswordEntered, text = model.password, placeholder = Nothing, label = Input.labelHidden "" }
            ]
        , row [ centerX ]
            [ Input.button [] { onPress = Just SubmitPassword, label = text "Validar" }
            ]
        , row [ centerX ]
            [ el [] (text (showCounterFromInt model.timeDifference))
            ]
        , row [ centerX ]
            (List.map (\e -> row [] [ Input.button [] { onPress = Just GetClue, label = text "Pista" }, el [] (text e.description) ]) model.availableClues)
        ]


getInputColor : Bool -> Color
getInputColor isInvalid =
    if isInvalid then
        red

    else
        white


red =
    rgb 255 0 0


white =
    rgb 255 255 255


clueButtonView : Bool -> Element Msg
clueButtonView clueAvailable =
    if clueAvailable then
        Input.button [] { onPress = Just NextStep, label = text "Pedir pista" }

    else
        none


getTimeDifferenceToPosix : Maybe Int -> Time.Posix
getTimeDifferenceToPosix time =
    Time.millisToPosix (Maybe.withDefault 0 time)


showTimeFromInt : Maybe Int -> (Time.Zone -> Time.Posix -> Int) -> String
showTimeFromInt time timeToInt =
    String.padLeft 2 '0' <| String.fromInt <| timeToInt Time.utc <| getTimeDifferenceToPosix time


showCounterFromInt : Maybe Int -> String
showCounterFromInt time =
    showTimeFromInt time Time.toMinute ++ ":" ++ showTimeFromInt time Time.toSecond
