module VideoPage exposing (..)

import Model exposing (Model)
import Element exposing (..)
import Element.Input as Input
import Html exposing (Html)
import Html.Attributes as Attributes
import Messages exposing (..)
import Events exposing (..)

videoView : Model -> String -> Element Msg
videoView model src =
    column [ centerX, height fill, width fill, spaceEvenly ]
        [ row [ centerX ]
            [ html
                (Html.video [ Attributes.autoplay True, Attributes.controls True, onEnded VideoEnded ]
                    [ Html.source [ Attributes.src src ] []
                    ]
                )
            ]
        , row [ centerX ]
            [ Input.button [] { onPress = Just NextStep, label = text "Comenzar" }
            ]
        ]
