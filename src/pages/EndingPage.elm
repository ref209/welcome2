module EndingPage exposing (..)

import Model exposing (Model)
import Messages exposing (..)
import Element exposing (..)

endingView : Model -> Element Msg
endingView model =
    column [ centerX, height fill, width fill, spaceEvenly ]
        [ row [ centerX ]
            [ el [] (text "End")
            ]
        ]
