module Step exposing (..)

type Step
    = Configuration
    | Landing
    | Intro
    | Game
    | Outro
    | Ending
