module Challenge exposing (Challenge)

import Clue exposing (Clue)


type alias Challenge =
    { description : String
    , password : String
    , clues : List Clue
    }
