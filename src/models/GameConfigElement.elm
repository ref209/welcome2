module GameConfigElement exposing (GameConfigElement)

import Challenge exposing (Challenge)

type alias GameConfigElement =
    { location : String
    , numberOfPlayers : String
    , challenges : List Challenge
    }
