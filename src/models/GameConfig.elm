module GameConfig exposing (GameConfig)

import GameConfigElement exposing (GameConfigElement)

type alias GameConfig =
    { locations : List String
    , numberOfPlayers : List String
    , videoIntroSrc : String
    , videoOutroSrc : String
    , timerForCluesInMinutes : Int
    , gameConfigs: List GameConfigElement
    }
