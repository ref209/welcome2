module Model exposing (Model)

import Clue exposing (Clue)
import GameConfig exposing (GameConfig)
import GameConfigElement exposing (GameConfigElement)
import Step exposing (Step)
import Time


type alias Model =
    { step : Step
    , selectedLocation : String
    , selectedNumberOfPlayers : String
    , gameConfig : GameConfig
    , selectedGameConfig : Maybe GameConfigElement
    , currentChallengeIndex : Int
    , password : String
    , isPasswordInvalid : Bool
    , timestampAtStart : Maybe Time.Posix
    , timeDifference : Maybe Int
    , availableClues : List Clue
    }
