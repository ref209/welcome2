module Messages exposing (..)

import Time
import Http
import GameConfig exposing (GameConfig)

type Msg
    = NextStep
    | GotGameConfig (Result Http.Error GameConfig)
    | SelectLocation String
    | SelectNumberOfPlayers String
    | SubmitConfiguration
    | VideoEnded
    | PasswordEntered String
    | CurrentTime Time.Posix
    | Tick Time.Posix
    | GetClue
    | SubmitPassword
