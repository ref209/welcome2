module GameConfigDecoder exposing (..)

import Challenge exposing (Challenge)
import Clue exposing (Clue)
import GameConfig exposing (GameConfig)
import GameConfigElement exposing (GameConfigElement)
import Json.Decode exposing (Decoder, field, int, list, string, succeed)
import Json.Decode.Pipeline exposing (required)


gameConfigDecoder : Decoder GameConfig
gameConfigDecoder =
    succeed GameConfig
        |> required "locations" (list string)
        |> required "numberOfPlayers" (list string)
        |> required "videoIntroSrc" string
        |> required "videoOutroSrc" string
        |> required "timerForCluesInMinutes" int
        |> required "gameConfig" (list gameConfigElementDecoder)


gameConfigElementDecoder : Decoder GameConfigElement
gameConfigElementDecoder =
    succeed GameConfigElement
        |> required "location" string
        |> required "numberOfPlayers" string
        |> required "challenges" (list challengeDecoder)


challengeDecoder : Decoder Challenge
challengeDecoder =
    succeed Challenge
        |> required "description" string
        |> required "password" string
        |> required "clues" (list clueDecoder)


clueDecoder : Decoder Clue
clueDecoder =
    succeed Clue
        |> required "description" string
